$BaseDir = (Split-Path $script:MyInvocation.MyCommand.Path)

# Includes all private scripts
# All private scripts & modules start with __
Get-ChildItem $BaseDir -Filter '__*.ps1' -Recurse | ForEach-Object { 
    . $_.FullName
}

# Loading Scripts
. $BaseDir\Alias.ps1

Import-Module Utils/Utils.psm1

# oh-my-posh
$theme = "/pure.omp.json"
oh-my-posh --init --shell pwsh --config $env:POSH_THEMES_PATH$theme | Invoke-Expression
