# FOR PUBLIC MODULES: $script:MyInvocation.MyCommand.Path)\Public\*

# Loading all module functions in directory.
Get-ChildItem (Split-Path $script:MyInvocation.MyCommand.Path) -Filter '*.ps1' -Recurse | ForEach-Object { 
    . $_.FullName
}