function Get-IPBlockedBy {
    [CmdletBinding()]
    param (
        [Parameter(
            Mandatory = $true,
            ValueFromPipeline = $true,
            Position = 0,
            HelpMessage = "Port number to look for processes uisng the IP address.")]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        [Int]
        $Port
    )
    process {
        $ownedBy = Get-NetTCPConnection 
        | Where-Object Localport -eq $Port 
        | Select-Object LocalPort, OwningProcess

        if ($ownedBy) {
            $ownerProcessInformation = (Get-Process -Id $ownedBy.OwningProcess | Select-Object ProcessName, CommandLine, Path)
            Add-Member -InputObject $ownerProcessInformation -Name "LocalPort" -Value ($ownedBy.LocalPort) -MemberType NoteProperty
            Add-Member -InputObject $ownerProcessInformation -Name "OwningProcess" -Value ($ownedBy.OwningProcess) -MemberType NoteProperty
        
            Write-Output $ownerProcessInformation
        } else {
            Write-Warning -Message "Process using Port($Port) not found."
            Write-Host "There is no process using the provided port."
        }
    }
}

Export-ModuleMember -Function Get-IPBlockedBy