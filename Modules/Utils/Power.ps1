function Set-PowerState {
    param (
        [Parameter(Mandatory, HelpMessage = "Set the power state")]
        [ValidateSet("Suspend", "Poweroff")]
        [String]
        $State
    )
    process {
        Add-Type -AssemblyName System.Windows.Forms
        $SleepState = [System.Windows.Forms.PowerState]::Suspend
        $Poweroff = "Poweroff"
        # $HibernateState = [System.Windows.Forms.PowerState]::Hibernate
        
        switch ($State) {
            "$SleepState" { 
                [System.Windows.Forms.Application]::SetSuspendState($SleepState, $false, $false)
            }
            "$Poweroff" {
                Stop-Computer
            }
            Default {
                Write-Host "Wonderful: You can't seem to figure out what you want."
            }
        }
        
    }
}

Export-ModuleMember -Function Set-PowerState