# Aliase _w parameters
# Get all files including hidden files
function Get-AllChildItem { Get-ChildItem -Force }

# Aliases
Set-Alias -Name ll -Value Get-AllChildItem
Set-Alias -Name k -Value kubectls